var gulp = require("gulp"),
    browserSync = require("browser-sync"),
    browserify = require("browserify"),
    source = require("vinyl-source-stream"),
    mochaPhantomJS = require("gulp-mocha-phantomjs");

gulp.task("browser-sync", function () {
    "use strict";
    browserSync({
        server: {
            //serveur de test
            baseDir: ["./test/", "./"],
            //assigner tests.html comme index du serveur
            index: "tests.html"
        }
    });
});

gulp.task("browserify", function() {
    "use strict";
    return browserify("./test/tests.js")
        .bundle()
        .on("error", function (err) {
            console.log(err.toString());
            this.emit("end");
        })
        .pipe(source("tests-browserify.js"))
        .pipe(gulp.dest("test/"))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task("test", function () {
    "use strict";
    return gulp.src("./test/tests.html")
        .pipe(mochaPhantomJS());
});

gulp.task("serve", ["browserify", "browser-sync"], function () {
    "use strict";
    //observeur sur les modifications du fichier de tests
    gulp.watch(["test/tests.js", "src/text-changer.js"], ["browserify", "test"]);
});