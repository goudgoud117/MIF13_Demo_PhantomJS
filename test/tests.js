var assert = require("assert"),
    changeText = require("../src/text-changer.js");

describe('TextChanger', function(){
    var element = document.createElement("section");
    element.appendChild(
        document.createElement("span")
            .appendChild(
                document.createTextNode("Remplacer moi")
            )
    );

    describe('#changeText(element, text)', function() {
        it('Cela devrait remplacer le contenu d un element avec le contenu fournit', function() {
            changeText().replaceText(element, "test");

            assert.equal(element.childNodes[0].nodeValue, "test");
        });

        it('Cela doit renvoyer une erreur si ce n est pas un element du DOM', function() {
        assert.throws(function () {
            changeText().replaceText(null, "test");
        }, /DOM element/);
        });

    });

});