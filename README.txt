Hello partenaire de mif13, voici un petit guide d'installation pour notre démo de mardi :

installer node et npm si pas déja fait :

1)
apt-get update
apt-get install nodejs
apt-get install npm

2) Création de lien symbolique :

sudo ln -s /usr/bin/nodejs /usr/local/bin/node
sudo ln -s /usr/bin/npm /usr/local/bin/npm

3) Installer gulp (dependance node)

npm install -g gulp

4) Puis
npm install
OU
npm install --save browser-sync browserify gulp gulp-mocha-phantomjs mocha-phantomjs mocha vinyl-source-stream

5) TADA

II-Utilisation

Lancement du serveur gulp:

gulp serve

Lancement de test avec mocha (depuis la racine du projet):

mocha test/tests.js (cf script dans package.json)